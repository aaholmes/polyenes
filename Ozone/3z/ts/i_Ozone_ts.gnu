set title "States of Ozone TS geom using ANO-L-pVTZ basis and optimized orbitals"
set xlabel "E_{var}-E_{tot} (Ha)" font "Times-Roman,20"
set ylabel "E_{tot} (Ha)" font "Times-Roman,20" offset 2
#set key top left
#set key at graph .32,.99 spacing .6 font "Times-Roman,12"
set key at graph 0.95,.25 spacing 1.2 samplen 1 font "Times-Roman,18"
set style data p
set format y "%6.3f"
set ytics 0.001
set mytics 5

e_t_1(x)=e_t_1_0+abs(e_t_1_1)*x+e_t_1_2*x**2
e_t_2(x)=e_t_2_0+abs(e_t_2_1)*x+e_t_2_2*x**2

fit e_t_1(x) "<paste energy_var_Ozone_ts energy_tot_Ozone_ts" using ($4-$8):8 via e_t_1_0, e_t_1_1, e_t_1_2
fit e_t_2(x) "<paste energy_var_Ozone_ts energy_tot_Ozone_ts" using ($5-$11):($11-0.00034) via e_t_2_0, e_t_2_1, e_t_2_2


plot [0:][:] \
     '<paste energy_var_Ozone_ts energy_tot_Ozone_ts' u ($4-$8):8 w p lc rgb "red" ps 1. t "1^1A_g  E_{tot}", \
     e_t_1(x) lc rgb "red" notitle, \
     '<paste energy_var_Ozone_ts energy_tot_Ozone_ts' u ($5-$11):($11-0.00034) w p lc rgb "green" ps 1. t "2^1A_g  E_{tot} - 0.00034", \
     e_t_2(x) lc rgb "green" notitle

set print "energy_extrap_Ozone_ts"
print e_t_1_0, e_t_2_0

set size 1.,1.; set term post eps enhanced color solid "Times-Roman" 20 ; set output 'energy_Ozone_ts.eps' ; replot
