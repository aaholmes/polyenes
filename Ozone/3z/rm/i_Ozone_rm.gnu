set title "States of Ozone RM germ using ANO-L-pVTZ basis and optimized orbitals"
set xlabel "E_{var}-E_{tot} (Ha)" font "Times-Rrman,20"
set ylabel "E_{tot} (Ha)" font "Times-Rrman,20" offset 2
#set key top left
#set key at graph .32,.99 spacing .6 font "Times-Rrman,12"
set key at graph 0.35,.85 spacing 1.2 samplen 1 font "Times-Rrman,18"
set style data p
set format y "%6.3f"
set ytics 0.001
set mytics 5

e_t_1_0=-225.091
e_t_2_0=-225.091
e_t_2_1=0.9
e_t_2_2=0.1

e_t_1(x)=e_t_1_0+abs(e_t_1_1)*x+e_t_1_2*x**2
e_t_2(x)=e_t_2_0+abs(e_t_2_1)*x+e_t_2_2*x**2

fit e_t_1(x) "<paste energy_var_Ozone_rm energy_tot_Ozone_rm" using ($4-$8):8 via e_t_1_0, e_t_1_1, e_t_1_2
fit [0:0.05] e_t_2(x) "<paste energy_var_Ozone_rm energy_tot_Ozone_rm" using ($5-$11):($11-0.22539) via e_t_2_0, e_t_2_1, e_t_2_2


plot [0:0.05][-225.092:] \
     '<paste energy_var_Ozone_rm energy_tot_Ozone_rm' u ($4-$8):8 w p lc rgb "red" ps 1. t "1^1A_g  E_{tot}", \
     e_t_1(x) lc rgb "red" notitle, \
     '<paste energy_var_Ozone_rm energy_tot_Ozone_rm' u ($5-$11):($11-0.22539) w p lc rgb "green" ps 1. t "2^1A_g  E_{tot} - 0.22539", \
     e_t_2(x) lc rgb "green" notitle

set print "energy_extrap_Ozone_rm"
print e_t_1_0, e_t_2_0

set size 1.,1.; set term post eps enhanced color solid "Times-Rrman" 20 ; set output 'energy_Ozone_rm.eps' ; replot
