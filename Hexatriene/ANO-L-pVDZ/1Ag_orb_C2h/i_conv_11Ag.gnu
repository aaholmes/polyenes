set title "Energy conv. of 22-e (frozen-core) 1^1A_g Hexatriene ANO-L-pVDZ basis optim orbs" font "Times-Roman,18"
# set style data linespoints
set xlabel "E_{var}-E_{tot} (Ha)" font "Times-Roman,28"
set ylabel "E_{tot} (Ha)" font "Times-Roman,28" offset 2
set format y "%.3f"
set key top left
set samples 2000
set ytics 0.005
set mytics 5

set print "energy_extrap_Hexatriene_11Ag"

e_v_0  = -232.5584

e_v(dele)=e_v_0 + e_v_1*dele + e_v_2*dele**2

fit e_v(x) "<paste energy_var energy_tot" using ($4-$8):4 via e_v_0, e_v_1, e_v_2

e_t_0  = -232.5584

e_t(dele)=e_t_0 + e_t_1*dele + e_t_2*dele**2

fit e_t(x) "<paste energy_var energy_tot" u ($4-$8):8 via e_t_0, e_t_1, e_t_2

plot [0:0.11][-232.76:-232.62]\
 '<paste energy_var energy_tot' u ($4-$8):4 w lp lc rgb "red" t "E_{var}", \
 [0:] e_v(x) lc rgb "red" notitle, \
 '<paste energy_var energy_tot' u ($4-$8):8 w lp lc rgb "blue" t "E_{tot}", \
 [0:] e_t(x) lc rgb "blue" notitle

print e_v_0, e_t_0

set size 1.,1.; set term post eps enhanced color solid "Times-Roman" 20 ; set output 'convergence_Hexatriene_11Ag.eps' ; replot
