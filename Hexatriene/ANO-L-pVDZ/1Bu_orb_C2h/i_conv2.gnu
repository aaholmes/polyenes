set title "Energy conv. of 32-e (frozen-core) ^1B_u Hexatriene ANO-L-pVDZ basis optim C_{2h} orbs" font "Times-Roman,18"
# set style data linespoints
set xlabel "E_{var}-E_{tot} (Ha)" font "Times-Roman,28"
set ylabel "E_{tot} (Ha)" font "Times-Roman,28" offset 2
set format y "%.3f"
set key top left
set samples 2000
set ytics 0.005
set mytics 5
set print "energy_extrap_1Bu"

e_v_0  = -232.6

e_v(dele)=e_v_0 + e_v_1*dele + e_v_2*dele**2

fit [0:] e_v(x) "energy" using ($4-$6):4 via e_v_0, e_v_1, e_v_2

e_t_0  = -232.6

e_t(dele)=e_t_0 + e_t_1*dele + e_t_2*dele**2

fit [0:] e_t(x) "energy" u ($4-$6):6 via e_t_0, e_t_1, e_t_2

plot [0:][-232.555:-232.440]\
 'energy' u ($4-$6):4 w p lc rgb "red" t "E_{var}", \
 [0:] e_v(x) lc rgb "red" notitle, \
 'energy' u ($4-$6):6 w p lc rgb "blue" t "E_{tot}", \
 [0:] e_t(x) lc rgb "blue" notitle


#'energy_var' u 1:4 lc 1 t 'Variational',\
#'energy_tot' u 1:3:5 w errorbars lc 1 t 'Total', \
#e_v(x) lc 1 notitle, \
#f(x) lc 1 notitle

#<paste energy_var_dele=5e-5_Lz energy_tot_dele=5e-5_Lz energy_var_dele=2.5e-5_Lz energy_tot_dele=2.5e-5_Lz' u 1:($21-($21-$6)*($26-$21)/($26-$21-$12+$6))
#smooth csplines lc rgb "cyan" t "E_{var}(extrap) L_z", \

print e_v_0, e_t_0

set size 1.,1.; set term post eps enhanced color solid "Times-Roman" 20 ; set output 'convergence_Hexatriene_1Bu_Cs_orbs.eps' ; replot
