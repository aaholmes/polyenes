\documentclass[10pt,aps,prb,twocolumn,amsmath,amssymb,superscriptaddress]{revtex4-1}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{color}
\usepackage{multirow}
\usepackage[caption = false]{subfig}

\def\beq{\begin{eqnarray}}
\def\eeq{\end{eqnarray}}
\def\beqq{\begin{eqnarray*} \color{blue} }
\def\eeqq{\end{eqnarray*}}
\def\OpOne{\hat{\mathbf{1}}}
\def\rvec{{\mathbf r}}
%\def\V{{\cal V}}
%\def\C{{\cal C}}
\def\V{\mathcal{V}}
\def\C{\mathcal{C}}
\def\NMC{{N_{\mathrm{MC}}}}
\def\NpMC{{N'_{\mathrm{MC}}}}
\def\NppMC{{N''_{\mathrm{MC}}}}
\def\NMCdiff{{N_{\mathrm{MC}}^{\mathrm{diff}}}}
\def\Nd{{N_d}}
\def\Nddiff{{N_{d}^{\mathrm{diff}}}}
\newcommand{\bra}[1]{\left\langle #1 \right|}
\newcommand{\ket}[1]{\left|  #1  \right\rangle}
\def\eh{{\rm E}_h}
\def\meh{{\rm mE}_h}


\begin{document}

\title{Excited States of Methylene, Polyenes, and Ozone\\from Heat-bath Configuration Interaction}

%Alan D.Chien,1 Matthew Otten,2 Adam A.Holmes,2,3 C.J.Umrigar,2  Sandeep Sharma and Paul M.Zimmerman1

\author{Alan D. Chien}
\affiliation{Department of Chemistry, University of Michigan, Ann Arbor,MI 48109,USA}

\author{Adam A. Holmes}
\affiliation{Department of Chemistry and Biochemistry, University of Colorado Boulder, Boulder, CO 80302, USA}
\affiliation{Laboratory of Atomic and Solid State Physics, Cornell University, Ithaca, NY 14853, USA}

\author{Matthew Otten}
\affiliation{Laboratory of Atomic and Solid State Physics, Cornell University, Ithaca, NY 14853, USA}

\author{C. J. Umrigar}
\affiliation{Laboratory of Atomic and Solid State Physics, Cornell University, Ithaca, NY 14853, USA}

\author{Sandeep Sharma}
\affiliation{Department of Chemistry and Biochemistry, University of Colorado Boulder, Boulder, CO 80302, USA}

\author{Paul M. Zimmerman}
\affiliation{Department of Chemistry, University of Michigan, Ann Arbor,MI 48109,USA}

\begin{abstract}
The electronically excited states of methylene, ethylene, butadiene, hexatriene, and ozone have long proven challenging due to their complex
mixtures of strong and weak correlations. To provide accurate benchmark energies for these states, semistochastic heat-bath configuration
interaction is herein used to approximate full configuration interaction (FCI) energies.
%A new metric, the fraction of correlation energy obtained by the perturbative correction, is shown to be a good indicator of convergence. (I
don't think we need this?)
The presented butadiene/ANO-L-pVTZ and hexatriene/ANO-L-pVDZ computations are the largest FCI-level simulations to date, with FCI dimensions
exceeding $10^{35}$ and $10^{38}$ determinants. The best heat-bath CI estimates of the vertical excitation energies in butadiene/ANO-L-pVTZ are
6.73 eV for $2^1{\rm A}_{\rm g}$, and 6.37 eV for $1^1{\rm B}_{\rm u}$ (6.74 and 6.57 eV with ANO-L-pVDZ). The same gaps in hexatriene/ANO-L-pVDZ
are estimated to be 5.77 and 5.62 eV, respectively.
\end{abstract}

\maketitle

\section{Introduction}

%Full configuration interaction (FCI) provides the theoretically exact electronic energy in a given
one-particle basis set, making FCI calculations the benchmarks against which other quantum chemistry methods are
evaluated~\cite{Knowles1984,Olsen1988,Olsen1990,Olsen1996,Rossi1999,Dutta2003,Gan2006}.
The exponential increase in Hamiltonian dimension with increasing system size means that traditional full
configuration interaction (FCI) benchmarks are not easily achievable for molecular systems larger than simple
diatomics~\cite{Knowles1984,Olsen1988,Olsen1990,Olsen1996,Rossi1999,Dutta2003,Gan2006} in small basis sets.
Recent years have seen impressive progress in the development of approximate methods that produce FCI-quality energies for much
larger systems, such as  density matrix renormalization group (DMRG)~\cite{White1993,White1999,Chan2002,Chan2011,Olivares-Amaya2015},
FCI quantum Monte Carlo (FCIQMC)~\cite{Booth2009,Cleland2010,PetHolChaNigUmr-PRL-12,Booth2013}, and the recent incremental FCI
(iFCI)~\cite{Zimmerman2017,Zimmerman2017a,Zimmerman2017b}.

Recent years have also seen a revival of interest in select CI (SCI) algorithms. SCI iteratively builds a subspace Hamiltonian %~\cite{Chien2017}
({\color{red} wrong citation?})
composed of the most important determinants of the wave function, which are generally selected based on perturbative coefficients. Once
a variational wave function is obtained, perturbation theory is usually used to improve the variational energy. The first such
Selected CI Plus Perturbation Theory (SCI+PT) method was called Configuration Interaction by Perturbatively Selecting Iteratively
(CIPSI)~\cite{Huron1973,Buenker1975,Buenker1978,Evangelisti1983,Harrison1991,BenAmor2011}. CIPSI becomes expensive once the variational space
grows large enough, since at each iteration, the algorithm generates and evaluates all determinants connected to the current variational
subspace by nonzero Hamiltonian matrix elements.
%One way to reduce the number of connections is to only consider excitations from a subset of the currently selected determinants, as in
the original algorithm for generating the deterministic subspace in semistochastic FCIQMC~\cite{PetHolChaNigUmr-PRL-12} and in the recent
adaptive sampling CI (ASCI)~\cite{Tubman2016} algorithm. Despite this minor improvement, the requirement to generate and evaluate all connected
determinants remained a bottleneck.

Heat-bath CI (HCI)~\cite{Holmes2016} was designed to eliminate this problem by only considering determinants which pass its new, more simple
importance measure, which avoids having to check all connected determinants.
HCI was further improved by semistochastic evaluation of the PT energy in semistochastic HCI (SHCI)~\cite{Sharma2017}, which eliminated
the memory bottleneck coming from having to store the very large number of perturbative determinants in memory.
%  Because FCI is a benchmark-level method with extreme cost, results from FCI are typically most useful in evaluating other electronic structure
methods. Unfortunately, benchmarks for polyatomics have largely been unavailable, and thus the subtle effects of correlations amongst numerous
electrons have not been accurately quantified.
With SHCI, ground and excited states~\cite{holmes2017excited} of systems of substantial size $-$ at least compared to prior algorithms $-$
can now be treated at the FCI level.

Our aim in this paper is to provide highly accurate benchmarks for electronically excited states of the challenging
polyatomics in Figure 1, using SHCI. We first present calculations on methylene as the first test case, due to its small size
yet challenging electronic structure~\cite{Sherrill1998}. Next, we investigate ozone due to its role in upper atmosphere
chemistry~\cite{Theis2016}. Finally, we examine the first few polyenes $-$ ethylene, butadiene, and hexatriene $-$ long
studied for their role as prototypical organic conducting polymers. The low-lying valence excited states, $2^1{\rm A}_{\rm g}$
and $1^1{\rm B}_{\rm u}$, are especially challenging to characterize and have long been a target of accurate electronic structure
theories~\cite{Tavan1987,Watts1996,Starcke2006,Li1999,Nakayama1998,Schreiber2008,Mazur2009,Schmidt2012,Piecuch2015}. %With $10^{35}$ and $10^{38}$
determinants in the FCI space of butadiene/ANO-L-pVTZ and hexatriene/ANO-L-pVDZ, respectively, FCI-level computations are incredibly challenging,
and will push SHCI towards its limit.

%Figure 1

  This article is organized as follows. In Section II, we review the excited-state SHCI algorithm and a technique for extrapolating SHCI
  energies to the FCI limit. Section III presents SHCI results for methylene, ozone, ethylene, butadiene, and hexatriene. Small systems in which
  benchmark values can be obtained (methylene, ethylene, and ozone/cc-pVDZ) are first examined to obtain insights into the convergence behavior
  of SHCI. These observations are then used to estimate the convergence level of ozone/cc-pVTZ, butadiene, and hexatriene results. Section IV
  provides conclusions and an outlook on the SHCI method for excitation energy benchmarking.

\section{Semistochastic Heat-bath Configuration Interaction}
  As Heat-bath Configuration Interaction and semistochastic perturbation theory have been described in
  detail~\cite{Holmes2016,Sharma2017,HolUmrSha-JCP-17}, only a brief overview will be given here. The HCI algorithm can be divided into
  variational and perturbative stages, each of which selects determinants through threshold values, $\epsilon_V$ and $\epsilon_{PT}$,
  respectively. The variational space ($\V$) contains the determinants included in the current subspace, and the connected space ($\C$)
  contains all determinants connected by single or double excitations to $\V$.

  The variational stage iteratively adds determinants to $\V$ by
\begin{enumerate}
\item Adding all determinants $a$ connected to determinants in the current $\V$ that pass the importance criterion $\max_i \left|H_{ai}
\max_n\left(\left|c_i^n\right|\right)\right| > \epsilon_V$, where $c_i^n$ is the coefficient of determinant $i$ in state $n$.
\item Constructing the Hamiltonian and solving for the roots of interest, in the basis of all determinants in the newly expanded $\mathcal{V}$.
\item Repeating 1-2 until convergence.
\end{enumerate}
  After convergence of $\mathcal{V}$, signified by few additional determinants or small variational energy changes, a second-order perturbative
  energy correction for the variational energy $E$ is calculated by

\beq
\Delta E_2 = \sum_{k\in\mathcal{C}} \frac{\left(\sum_{i\in\mathcal{V}}^{(\epsilon_{PT})} H_{ki} c_i\right)^2}{E_0-H_{kk}},
\eeq
where $k$ runs over all determinants in $\mathcal{C}$, and $i$ ranges over all determinants in $\mathcal{V}$. Similar to the variational stage,
the perturbation only considers the determinants connected to the final $\V$ space that have an importance measure greater than a parameter
$\epsilon_{PT}$, which is typically much smaller than $\epsilon_V$. The storage of the entire space of determinants used in  becomes a memory
bottleneck for larger systems. This memory bottleneck can be sidestepped by calculating the second-order perturbation correction semistochastically,
as in the SHCI algorithm~\cite{Sharma2017}. In SHCI, a deterministic energy correction is first calculated with a larger $\epsilon_{PT}^d$,
and the error from using the larger $\epsilon_{PT}^d$ is then calculated stochastically by taking the difference of the second-order corrections
done with $\epsilon_{PT}$ and $\epsilon_{PT}^d$. Samples are taken until the statistical error falls below a specified threshold.

\subsection{Error Analysis}
  The target accuracy for absolute or relative energies is typically chosen to be either within 1 mHa or 1.6 mHa (1 kcal/mol, representing
  chemical accuracy) of the FCI limit.
%{\color{red} Remove rest of paragraph?} A major obstacle in examining systems as large as hexatriene (FCI dimension $10^{38}$) is accurately
determining the extent of convergence relative to the FCI limit. Straightforward strategies, such as examining changes in energy between
increasingly accurate runs or extrapolating energies to the FCI limit, can be unreliable unless one is extremely close to the FCI limit. For
instance, varying  by a small amount may produce a small change in total energy, but this does not necessarily indicate convergence to the
true FCI energy. Furthermore, extrapolation procedures can result in significantly different energies depending on the available data and the
chosen fitting equation. As neither of these measures is a certain signal for convergence in the larger systems for which we cannot closely
approach the FCI limit (ozone/cc-pVTZ, butadiene, and hexatriene), this work utilizes a new convergence metric, the fraction of correlation
energy obtained by the perturbative correction (), to support the observed convergence of SHCI results.
%   can be used as a convergence metric because the error in total energy arises only from the perturbative correction, since the variational
treatment is exact. Therefore, if the perturbative correction is less than 1 mHa, the variational energy itself is converged to within 1 mHa
(we assume small  such that the perturbative correction itself is converged). Such small perturbative corrections are often unobtainable,
however, so an alternative accuracy measure is required for general application. For this alternative, we use , which utilizes the distribution
of the recovered correlation energy between variational and perturbative contributions, to indicate closeness to the FCI limit. As  falls,
so too should the error, and there may be a fraction at which total or relative energies are generally converged to the FCI limit. This claim
is examined and shown to be empirically true for relative energies in the smaller systems for which benchmark values can be obtained.
%   To use  for excited states, which are not well described by single electronic configurations, a slightly different definition of
correlation energy is required. Instead of taking the correlation energy as the energy difference between Hartree-Fock and FCI, we utilize
a minimal complete active space (CAS) wave function in place of Hartree-Fock, resulting in . This leads to a well-defined  for all states,
with  values for relative energies defined as the average  of the two involved states.

\subsection{Computational Details}

\section{Results}
  SHCI was used to investigate the low-lying valence states of methylene, ethylene, ozone, butadiene, and hexatriene. These systems contain 6, 12,
  18, 22, and 32 valence electrons, all of which will be correlated with the full virtual orbital space. %{\color{red} Remove rest of paragraph?}
  With no restriction on the CI excitation level, the massive combinatorial spaces of CI determinants make it difficult to guarantee convergence
  in systems with over 20 valence electrons. Therefore, the variational energies (), perturbative corrections (), and total () energies from a
  progression of  calculations down to benchmark levels will first be examined to determine meaningful convergence metrics for the larger molecules.

\subsection{Estimating Chemical Accuracy}
  SHCI can obtain benchmark energies fully converged to the FCI limit for the smaller test systems: methylene with aug-cc-pVQZ basis, ethylene
  with ANO-L-pVDZ and ANO-L-pVTZ bases, and ozone with cc-pVDZ. %For methylene with aug-cc-pVQZ and ethylene with ANO-L-pVDZ, perturbative
  corrections are sub-mHa. For ethylene with ANO-L-pVTZ and ozone with cc-pVDZ, perturbative corrections are 1.4 - 2.6 mHa, allowing accurate
  extrapolations to FCI.
%The small  indicates closeness to the FCI limit, which minimizes errors in extrapolation, allowing consistent energies to be returned  with
various fitting equations and data points.
%This is indeed the case (Supporting Information (SI) sections 2 and 3), with all extrapolated energies being within 0.4 mHa of the most
accurate obtained .

%  Methylene's convergence progression is well behaved due to having only six valence electrons (SI section 1). For ethylene, the relationship
between  and absolute error is presented in Figure 2 for ANO-L-pVTZ, while ANO-L-pVDZ results show similar trends, as seen in the SI section 2
and Table 1. Ethylene's absolute energies require an average  of 14.0\% and 12.1\% to become within 1.6 and 1 mHa of the converged SHCI limit,
respectively, whereas relative energies reach errors of 1.6 and 1 mHa with average  values of 14.8\% and 10.9\%. The relative energies require
slightly lower  (i.e. tighter ) to converge to sub-mHa accuracies. This is somewhat surprising as one expects energy gaps to converge faster
than absolute energies due to cancellation of errors. Closer examination shows that this behavior is due to the non-variational character of the
perturbative corrections, which allow  to converge to the FCI limit from below or above. This situation occurs for ethylene¿s $1^3{\rm B}_{\rm
1u}$ and 11Ag states, which converge to their FCI limits from above and below respectively, leading to slower convergence of the relative energies.

%Table 1
%Figure 2
%
%  Ozone in the cc-pVDZ basis and at its ground-state geometry is the next benchmark system. The most accurate run was accomplished with  Ha,
giving a 21A1-$1^1{\rm A}_{\rm 1}$ gap of 4.10 eV. With SHCI, the gap is converged to within 1 mHa when treating only 104 determinants out of
the $10^{16}$ in the FCI space. Figure 3 graphs the relationship between  and errors as in ethylene, where it can be seen that a small  of 2.8\%
is necessary to converge absolute energies to sub-mHa errors. The relative energies, however, converge more quickly, with  values of 21.3\%
and 13.8\% needed to obtain errors under 1.6 and 1 mHa, respectively.
%
%%Figure 3
%
%  The values of  that indicate convergence of  are compiled in Table 1. Absolute energy errors are inconsistently estimated by , as seen
by the large difference in  values for ozone compared to methylene and ethylene. The relationship between  and relative energies is better
behaved, with only minor variations in the  quantities signaling tight convergence. Relative energies are consistently converged to within 1
mHa with an average  = 10.8\% and to within 1.6 mHa with an average  = 16.4\%. The required  for convergence appears to rise as the number of
correlated electrons increases. Thus, it is possible that in the larger molecules, relative energies will converge at slightly higher  than
needed for the benchmark systems. Regardless, this work will utilize conservative values of  to signal convergence, with  values below 10\%
and 15\% taken to signal errors in relative energies below 1 and 1.6 mHa respectively.

\subsection{Methylene}
  Methylene is a prototypical test case for advanced electronic structure methods, being small enough to be amenable to
  canonical FCI benchmarks, yet still requiring accurate treatment of dynamic and static correlations for correct excitation
  energies~\cite{Schaefer1986,Bauschlicher1986,Sherrill1997,Zimmerman2009,Slipchenko2002,Shao2003,Chien2017}. The four lowest lying states
  of methylene vary in spin and spatial symmetry, 13B1, $1^1{\rm A}_{\rm 1}$, $1^1{\rm B}_{\rm 1}$, and $2^1{\rm A}_{\rm 1}$. With only six
  valence electrons, SHCI can be used with the large aug-cc-pVQZ basis. With  Ha, perturbative corrections were less than 0.01 mHa, indicating
  strict convergence of the SHCI values to the FCI limit. The SI section 1 shows methylene's adiabatic energy gaps are converged to sub-mHa
  accuracy at , so  represents tighter convergence than needed to reach 1 mHa accuracy. Table 2 shows the most accurate SHCI adiabatic energy
  gaps differ from experiment by about 0.01 eV.28 Comparing canonical FCI in the TZ2P basis with SHCI in the larger aug-cc-pVQZ basis shows
  differences of up to 0.144 eV, suggesting that large basis sets are necessary to fully describe correlation in methylene. This was first
  hinted at by diffusion Monte Carlo (DMC) results~\cite{Zimmerman2009}, which are less sensitive to basis set, that agree with SHCI to within
  about 0.02 eV. CR-EOMCC(2,3)D relative energies are also converged to within 1 mHa of the benchmark SHCI values, indicating that high-level
  coupled cluster calculations can correlate six electrons sufficiently to obtain FCI-quality energy gaps.

%Table 2
\begin{table}
\begin{tabular}{c|c}
\hline
State & SHCI Energy (Ha)\\
\hline
\hline
$1^3$B$_1$ & -39.08849(1)\\
$1^1$A$_1$ & -39.07404(1)\\
$1^1$B$_1$ & -39.03711(1)\\
$2^1$A$_1$ & -38.99603(1)\\
\hline
\end{tabular}
\caption{Total energies of states of methylene}
\end{table}

\begin{table*}
\begin{tabular}{c|c|c|c|c|c}
\hline
Gap & SHCI (eV) & CR-EOMCC(2,3)D (eV) & FCI (eV) & DMC (eV) & Exp (eV)\\
\hline
\hline
$1^1$A$_1-1^3$B$_1$ & 0.393 & 0.415 & 0.482 & 0.406 & 0.400\\
$1^1$B$_1-1^3$B$_1$ & 1.398 & 1.422 & 1.542 & 1.416 & 1.411\\
$2^1$A$_1-1^3$B$_1$ & 2.516 & 2.499 & 2.674 & 2.524 & $-$\\
\hline
\end{tabular}
\caption{Energies of excited state gaps in methylene}
\end{table*}

\subsection{Ethylene}
  Ethylene is a prototypical benchmark system for electronic excitations, including a difficult-to-characterize $1^1{\rm B}_{\rm u}$
  state. Although the $1^1{\rm B}_{\rm u}$ state is qualitatively well described by a ¿-¿* excitation, a quantitative description requires
  a thorough accounting of dynamic correlation between ¿ and ¿ electrons~\cite{Davidson1996,Muller1999,Angeli2010}. Here, SHCI is applied
  to the low-lying valence states, $1^1{\rm A}_{\rm g}$, $1^1{\rm B}_{\rm 1u}$ and $1^3{\rm B}_{\rm 1u}$, in the ANO-L-pVTZ basis. Table 3
  shows that SHCI total and relative energies compare favorably with previous FCIQMC~\cite{Daday2012} and iFCI~\cite{Zimmerman2017a} results
  (SI section 2 gives results in the smaller ANO-L-pVDZ basis). Table 3 also indicates that coupled cluster methods must account for more than
  triples excitations in order to accurately correlate this system, as CR-EOMCC(2,3)D relative energies show errors greater than 1.6 mHa with
  respect to the SHCI benchmark values. With SHCI, absolute energies within 1 mHa of the FCI limit were obtained at  Ha, where only $10^5$
  variational determinants were considered out of a FCI space of $10^{18}$. These results suggest that polyatomics with up to 12 valence
  electrons and triple-zeta basis sets can now be considered unchallenging for FCI-level approximation. SHCI benchmarks agree with prior
  simulations which indicate the $1^1{\rm B}_{\rm 1u}$ vertical excitation does not correspond to experimental band maxima~\cite{Zimmerman2017b}.

%Table 3
\begin{table}
\begin{tabular}{c|c}
\hline
State & SHCI Energy (Ha)\\
\hline
\hline
$1^1$A$_{\rm g}$ & -78.4381(1)\\
$1^1$B$_{\rm 1u}$ & -78.1424(1)\\
$1^3$B$_{\rm 1u}$ & -78.2693(1)\\
\hline
\end{tabular}
\caption{Total energies of states of ethylene}
\end{table}

\begin{table*}
\begin{tabular}{c|c|c|c|c|c}
\hline
Gap & SHCI (eV) & CR-EOMCC(2,3)D (eV) & FCI (eV) & DMC (eV) & Exp (eV)\\
\hline
\hline
$1^1$B$_{\rm 1u}-1^1$A$_{\rm g}$ & 8.05 & 8.25 & 8.06 & $-$ & 7.66\\
$1^3$B$_{\rm 1u}-1^1$A$_{\rm g}$ & 4.59 & 4.76 & $-$ & 4.64 & 4.3-4.6\\
\hline
\end{tabular}
\caption{Energies of excited state gaps in ethylene}
\end{table*}


\subsection{Ozone}
  Ozone's potential energy surfaces have held great interest due to its role in atmospheric chemistry~\cite{Andersen2013}. An interesting
  feature of these surfaces predicted by computational studies is the existence of a metastable ring geometry on the ground state
  surface~\cite{Hay1972}. A lack of experimental evidence for such a species has fueled multiple studies of the pathway leading to the ring
  species over the years~\cite{Lee1990,Qu2005,Xantheas1990,Xantheas1991,Atchity1997,Atchity1997a}. The most recent such study by Ruedenberg
  et al. utilizes multi-reference CI with up to quadruple excitations~\cite{Sherrill1998}, expending considerable effort on selecting and
  justifying an active space. To provide an accurate picture at critical points along the theorized pathway with even treatment of all valence
  electrons, SHCI is applied to ozone¿s $2^1{\rm A}_{\rm 1}$-$1^1{\rm A}_{\rm 1}$ gap with cc-pVTZ at the three geometries of interest shown in
  Figure~\ref{fig:ozone}: the equilibrium geometry (termed the open ring minimum (OM)), the hypothetical ring minimum (RM), and the transition
  state (TS) between these two.

%Figure 4
\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{energy_Ozone_ts}
\end{center}
\caption{}\label{fig:ozone}
\end{figure}

  Ozone in the cc-pVTZ basis is too large to reach sub-mHa perturbative corrections, as computations with the necessary  would exceed available
  memory. The  corrections for the best available SHCI calculation, at  Ha, range from 15.8-25.8 mHa, and the extrapolated energies differ by up
  to 2.6 mHa from the best SHCI values (SI section 3), indicating that sub-mHa accuracy in total energies was not obtained. % values of 2.8\%
  and under, however, are recovered for all geometries at  Ha, which is well below the metric established for chemical accuracy of relative
  energies (15\%).
Furthermore, graphing the energy gaps at various  (Figure 5) shows little variance as the tightest convergence limit is approached. The RM data
set in Figure 5 is truncated above  (i.e. ) because the $2^1{\rm A}_{\rm 1}$ wave function is qualitatively different at looser , leading to
large changes in the $2^1{\rm A}_{\rm 1}$-$1^1{\rm A}_{\rm 1}$ energy gap. This serves as a cautionary case, demonstrating that qualitatively
correct wave functions are not guaranteed at loose , as important groups of determinants may be left out. This issue was only seen with a
relatively large number of electrons correlated in a moderately large basis. In these cases, we recommend  to ensure reasonable convergence.

  In Table 4, the SHCI energy gaps are compared to Ruedenberg et al's MRCI results~\cite{Theis2016}. SHCI results mostly resemble MRCI estimates,
  except for the RM geometry, where the gaps differ by more than 1 eV. Due to the relatively tight convergence, these SHCI results should be
  taken as the current best $2^1{\rm A}_{\rm 1}$-$1^1{\rm A}_{\rm 1}$ vertical excitation energies at the OM, RM, and TS geometries. SHCI
  therefore also allows some insight into the nature of the RM species. Along the $1^1{\rm A}_{\rm 1}$ potential surface, the RM and TS
  geometries lie 1.30 eV and 2.40 eV, respectively, above the OM geometry. This large barrier suggests that electronic excitations in ozone
  are likely required to reach RM, but the RM species should be relatively stable with a 1.10 eV barrier to return to the OM geometry. To
  obtain these results, $10^8$ variational determinants out of $10^{23}$ in the FCI space were required, but no active space selection was needed.

%Table 4
\begin{table}
\begin{tabular}{c|c|c}
\hline
Geometry & SHCI (Ha) & MRCI (SDTQ)\\
\hline
\hline
OM & 4.13 & 3.54-4.63\\
RM & 6.16 & 7.35-8.44\\
TS & 0.04 & 0.05-0.16\\
\hline
\end{tabular}
\caption{Total energies of states of ozone/cc-pVTZ $2^1{\rm A}_{\rm 1}$ - $1^1{\rm A}_{\rm 1}$ gaps compared with Rudenberg et al. All values in eV.}
\end{table}


%Figure 5

\subsection{Shorter Polyenes: Butadiene and Hexatriene}
  Butadiene and hexatriene are part of the polyene series, long studied for their role as prototypical organic conducting polymers. The spacing
  of the low-lying valence excited states has proven especially challenging to electronic structure methods.30¿38 In butadiene and hexatriene,
  SHCI is thus applied to the $1^1{\rm A}_{\rm g}$, $1^1{\rm B}_{\rm 1u}$, $1^3{\rm B}_{\rm 1u}$, and $2^1{\rm A}_{\rm g}$ states to provide
  accurate benchmarks. Butadiene and hexatriene are of special interest because their $1^1{\rm B}_{\rm 1u}$ and $2^1{\rm A}_{\rm g}$ states
  are nearly degenerate, resulting in conflicting reports of state ordering at lower levels of theory. These systems are too large for the
  routine application of FCI-level methods, although limited FCIQMC~\cite{Daday2012} and DMRG~\cite{Olivares-Amaya2015} studies have been
  performed on butadiene.

\subsubsection{Butadiene}
  Butadiene (C$_4$H$_6$) has FCI spaces of $10^{26}$ and $10^{35}$ determinants in the ANO-L-pVDZ and pVTZ basis sets, respectively, putting
  it at the edge of accessibility for modern FCI-quality approximations~\cite{Olivares-Amaya2015,Daday2012}. In the triple-zeta basis at  Ha,
  the resulting variational space of $10^8$ determinants leads to the perturbative corrections of $\sim$60 mHa and  (Table 5). In butadiene,
  all states approach the FCI limit from above, allowing cancellation of errors to produce accurate energy gaps. % for  above the recommended 10\%.
Figure 6 provides further evidence that the energy gaps are converged, as they vary by less than 1 mHa as the tightest convergence level ()
is approached.

  For the $1^1{\rm A}_{\rm g}$ ground state in an ANO-L-pVDZ basis, SHCI results can be compared in more detail to related FCI-level approximations
  in the literature. ANO-L-pVDZ¿s $1^1{\rm A}_{\rm g}$ state energy of -155.5528 is within 4 mHa of the most accurate estimates, a DMRG
  result of -155.557215 and an iFCI value of -155.5567 Ha.10 Linear extrapolation of the energies slightly overshoots these targets, producing
  -155.5592 Ha (SI section 4). %These results affirm that the absolute energy errors are not well predicted by the  metric (c.f. Table 1),
  which is 6.3\% at the tightest  ().

%Figure 6
\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{energy_Butadiene_2z}
\end{center}
\caption{}\label{fig:ozone}
\end{figure}

  Table 5 shows that SHCI butadiene energy gaps are similar to previous high-level theoretical calculations. For ANO-L-pVTZ, the calculated
  13Bu-$1^1{\rm A}_{\rm g}$ gap at is just 0.03 eV away from that computed by iFCI, and the $1^1{\rm B}_{\rm u}$-$1^1{\rm A}_{\rm g}$ gap is
  within 0.01 eV of FCIQMC. Since the $2^1{\rm A}_{\rm g}$-$1^1{\rm A}_{\rm g}$ energy gap does not currently have other FCI-level benchmarks,
  the SHCI values represent the current best theoretical estimate for the $2^1{\rm A}_{\rm g}$-$1^1{\rm A}_{\rm g}$ energy gap in butadiene, 6.73
  eV. The small gap between the $2^1{\rm A}_{\rm g}$ and $1^1{\rm B}_{\rm u}$ states is consistent with recent theoretical~\cite{Komainda2017}
  and experimental~\cite{Fuss2001} investigations demonstrating ultrafast population transfer from $1^1{\rm B}_{\rm u}$ to $2^1{\rm A}_{\rm g}$,
  which implies close proximity of the two states. SHCI¿s relative ordering of the $2^1{\rm A}_{\rm g}$ / $1^1{\rm B}_{\rm u}$ states agrees
  qualitatively with EOM-CC results~\cite{Watson2012}, but the $2^1{\rm A}_{\rm g}$-$1^1{\rm A}_{\rm g}$ gaps differ by 0.36 eV, indicating
  that the doubly-excited character of $2^1{\rm A}_{\rm g}$ was treated inadequately in the EOM-CC approach. As in ethylene, the $1^1{\rm
  B}_{\rm u}$-$1^1{\rm A}_{\rm g}$ gap differs from experiment by 0.45 eV, agreeing with prior indications that experimental band maxima of
  butadiene do not correspond to the vertical excitation energy~\cite{Watson2012}.

%Table 5

\subsubsection{Hexatriene}
  Hexatriene is at the current frontier of FCI-level computations, with a demanding FCI space of $10^{38}$ determinants in a double-zeta
  basis. Only one other algorithm, iFCI~\cite{Zimmerman2017a}, has approached FCI energies for such a large polyatomic. iFCI has estimated
  the singlet-triplet gap for hexatriene, but is not immediately applicable to singlet excited states. The current highest level computational
  estimates of valence energy gaps in hexatriene are therefore presented here.
%Hexatriene's relative energies have  values under 13.3\%, obtained by treating 108 determinants variationally at Ha. This value is well below
the threshold of 15\% for chemical accuracy, and
Furthermore, all energies converge to the FCI limit from above, allowing for improved cancellation of errors. Indeed, the hexatriene energy
gaps shown in Figure 7 indicate tight convergence, with variations below chemical accuracy among relative energies for the tightest heat-bath
tolerances. In hexatriene, the $1^1{\rm B}_{\rm u}$ state qualitatively changes at , but the $1^1{\rm B}_{\rm u}$-$1^1{\rm A}_{\rm g}$ gap is
well-behaved after this point. As in butadiene, investigations of hexatriene photo dynamics77¿80 place $1^1{\rm B}_{\rm u}$ close in energy to
21Ag. At the vertical excitation geometry, SHCI places 21Ag above $1^1{\rm B}_{\rm u}$ with a small gap of only 0.15 eV. For the triplet state,
the SHCI $1^3{\rm B}_{\rm u}$-$1^1{\rm A}_{\rm g}$ gap agrees well with iFCI at the slightly smaller 6-31G* basis, differing by 0.06 eV (Table
6). Once again, the SHCI $1^1{\rm B}_{\rm u}$-$1^1{\rm A}_{\rm g}$ gap differs significantly from experiment,81 indicating that experimental
band maxima do not correspond to vertical excitation energies in hexatriene.

%Figure 7
%Table 6
\begin{table}
\begin{tabular}{c|c|c|c|c}
\hline
Gap & SHCI & CC & iFCI & Exp\\
\hline
\hline
$2^1{\rm A}_{\rm g}$-$1^1{\rm A}_{\rm g}$ & 5.73 &5.72 & $-$ & 5.21\\
$1^1{\rm B}_{\rm u}$-$1^1{\rm A}_{\rm g}$ & 5.61 &5.30 & $-$ & 4.95,5.13\\
$1^3{\rm B}_{\rm u}$-$1^1{\rm A}_{\rm g}$ & 2.86 &2.80 & 2.81& 2.61\\
\hline
\end{tabular}
\caption{Excitation energy gaps of hexatriene/ANO-L-pVDZ. All values in eV.}
\end{table}


\section{Conclusion}
  SHCI represents an important step forward for SCI+PT methods, enabling computations of FCI-quality energies in some of the largest systems to date.
%In order to determine convergence for these polyatomics, a new metric for evaluating SCI+PT energies was introduced. This metric, , was
empirically shown to be a reliable indicator of convergence in energy gaps, but not absolute energies.
SHCI  can reach systems as large as butadiene and hexatriene, where CI spaces of $10^7-10^8$ determinants were used to represent FCI spaces
of $10^{35}$ and $10^{38}$, respectively, and still produce excitation energies to chemical accuracy.

  SHCI achieves this success by treating all valence electrons on an equal footing, and recovering most of the correlation energy in a
  deterministic fashion. These properties give it advantages over DMRG, which relies on structuring the correlation into an effectively
  1-dimensional problem, and over FCIQMC, which relies heavily on stochastic sampling. While the computational scaling of SHCI is still
  exponential, its ability to systematically reach FCI-level energies (with decreasing $\left|\Delta E_2\right|$) means that ground states
  and excited states in polyatomics with up to $\sim 32$ electrons can now be studied with sub-mHa accuracy.



\begin{acknowledgements}
\end{acknowledgements}

\bibliographystyle{jchemphys}
\bibliography{SHCI_paper,hci} %,hci,chan,zhang}



\end{document}

