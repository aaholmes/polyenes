set title "Energy conv. of 22-e (frozen-core) 2^1A_g Butadiene ANO-L-pVDZ basis optim orbs" font "Times-Roman,18"
# set style data linespoints
set xlabel "E_{var}-E_{tot} (Ha)" font "Times-Roman,28"
set ylabel "E_{tot} (Ha)" font "Times-Roman,28" offset 2
set format y "%.3f"
set key top left
set samples 2000
set ytics 0.005
set mytics 5

set print "energy_extrap_Butadiene_21Ag"

e_v_0  = -155.5584

e_v(dele)=e_v_0 + e_v_1*dele + e_v_2*dele**2

fit e_v(x) "<paste energy_var energy_tot" using ($5-$11):5 via e_v_0, e_v_1, e_v_2

e_t_0  = -155.5584

e_t(dele)=e_t_0 + e_t_1*dele + e_t_2*dele**2

fit e_t(x) "<paste energy_var energy_tot" u ($5-$11):11 via e_t_0, e_t_1, e_t_2

plot [0:][-155.320:-155.245]\
 '<paste energy_var energy_tot' u ($5-$11):5 w lp lc rgb "red" t "E_{var}", \
 [0:] e_v(x) lc rgb "red" notitle, \
 '<paste energy_var energy_tot' u ($5-$11):11 w lp lc rgb "blue" t "E_{tot}", \
 [0:] e_t(x) lc rgb "blue" notitle

print e_v_0, e_t_0

set size 1.,1.; set term post eps enhanced color solid "Times-Roman" 20 ; set output 'convergence_Butadiene_21Ag.eps' ; replot
