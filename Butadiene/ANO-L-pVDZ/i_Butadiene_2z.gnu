set title "States of Butadiene using ANO-L-pVDZ basis and optimized orbitals"
set xlabel "E_{var}-E_{tot} (Ha)" font "Times-Roman,20"
set ylabel "E_{tot} (Ha)" font "Times-Roman,20" offset 2
#set key top left
#set key at graph .32,.99 spacing .6 font "Times-Roman,12"
set key at graph 0.95,.25 spacing 1.2 samplen 1 font "Times-Roman,18"
set style data p
set format y "%6.3f"
set ytics 0.005
set mytics 5

e_t_1(x)=e_t_1_0+abs(e_t_1_1)*x+e_t_1_2*x**2
e_t_2(x)=e_t_2_0+abs(e_t_2_1)*x+e_t_2_2*x**2
e_t_3Bu(x)=e_t_3Bu_0+abs(e_t_3Bu_1)*x+e_t_3Bu_2*x**2
e_t_1Bu(x)=e_t_1Bu_0+abs(e_t_1Bu_1)*x+e_t_1Bu_2*x**2

fit e_t_1(x) "<paste 1Ag_orb_C2h_stateav2/energy_var 1Ag_orb_C2h_stateav2/energy_tot" using ($4-$8):8 via e_t_1_0, e_t_1_1, e_t_1_2
fit e_t_2(x) "<paste 1Ag_orb_C2h_stateav2/energy_var 1Ag_orb_C2h_stateav2/energy_tot" using ($5-$11):($11-.2432) via e_t_2_0, e_t_2_1, e_t_2_2
fit e_t_3Bu(x) "<paste 3Bu_orb_Cs/energy_var 3Bu_orb_Cs/energy_tot" using ($4-$7):($7-.1247) via e_t_3Bu_0, e_t_3Bu_1, e_t_3Bu_2
fit e_t_1Bu(x) "<paste 1Bu_orb_C2h/energy_var 1Bu_orb_C2h/energy_tot" using ($4-$7):($7-.2384) via e_t_1Bu_0, e_t_1Bu_1, e_t_1Bu_2


plot [0:][:] \
     '<paste 1Ag_orb_C2h_stateav2/energy_var 1Ag_orb_C2h_stateav2/energy_tot' u ($4-$8):8 w lp lc rgb "red" ps 1. t "1^1A_g  E_{tot}", \
     e_t_1(x) lc rgb "red" notitle, \
     '<paste 1Ag_orb_C2h_stateav2/energy_var 1Ag_orb_C2h_stateav2/energy_tot' u ($5-$11):($11-.2432) w lp lc rgb "green" ps 1. t "2^1A_g  E_{tot} - 0.2432", \
     e_t_2(x) lc rgb "green" notitle, \
     '<paste 3Bu_orb_Cs/energy_var 3Bu_orb_Cs/energy_tot' u ($4-$7):($7-.1247) w lp lc rgb "blue" ps 1. t "1^3B_u  E_{tot} - 0.1247", \
     e_t_3Bu(x) lc rgb "blue" notitle, \
     '<paste 1Bu_orb_C2h/energy_var 1Bu_orb_C2h/energy_tot' u ($4-$7):($7-.2384) w lp lc rgb "magenta" ps 1. t "1^1B_u  E_{tot} - 0.2384", \
     e_t_1Bu(x) lc rgb "magenta" notitle

print e_t_1_0, e_t_2_0, e_t_3Bu_0, e_t_1Bu_0

set size 1.,1.; set term post eps enhanced color solid "Times-Roman" 20 ; set output 'energy_Butadiene_2z.eps' ; replot
#-----
#1Ag_orb_C2h_stateav   1Ag_orb_Ci      1Ag_orb_Ci_Sandeep            1Ag_orb_Ci_stateav  1Bu_orb_C2h_1Bu  3Au_orb_Cs   3Bu_orb_Cs  i_Butadiene_2z.gnu
#1Ag_orb_C2h  1Ag_orb_C2h_stateav2  1Ag_orb_Ci.old  1Ag_orb_Ci_Sandeep_notimerev  1Bu_orb_C2h         3Au_orb_C2h      3Bu_orb_C2h  Ag_orb_C2h

#>>> 155.559538830045-155.316269635834
#0.24326919421099547
#>>> 155.559538830045-155.321111392585 
#0.23842743745998973
#>>> 155.559538830045-155.434839729221
#0.12469910082398883
